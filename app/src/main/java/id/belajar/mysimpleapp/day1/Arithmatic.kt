package id.belajar.mysimpleapp.day1

import java.lang.ArithmeticException

fun sum(inputA: Int, inputB: Int): Int = inputA + inputB
fun subtraction(inputA: Int, inputB: Int): Int = inputA - inputB
fun multiply(inputA: Int, inputB: Int): Int = inputA * inputB
fun division(inputA: Int, inputB: Int) {
    var result : Int
    try {
        result = inputA / inputB
        println(result)
    } catch(e: ArithmeticException){
        println("must not devide ${e.message}")
    }
}

fun main() {
    val dataA = 2
    val dataB = 0

    println(sum(dataA,dataB))
    println(subtraction(dataA,dataB))
    println(multiply(dataA,dataB))
    division(dataA,dataB)
}