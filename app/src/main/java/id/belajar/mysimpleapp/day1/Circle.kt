package id.belajar.mysimpleapp.day1


import kotlin.math.PI
import kotlin.math.ceil
import kotlin.math.round


fun areaCircle(radius: Double): Double = PI * radius * radius
fun areaCircle(radius: Int): Double = PI * radius * radius
fun circumferenceCircle(radius: Double): Double = 2 * PI * radius
fun circumferenceCircle(radius: Int): Double = 2 * PI * radius

fun main() {
    val radiusA = 3.0
    val radiusB = 5.0
    val radiusC : Int = 3


    val areaCircleA = areaCircle(radiusA)
    val circumferenceCircleA = round(circumferenceCircle(radiusA))
    val areaCircleB = areaCircle(radiusB)

    println("Area Circle A: $areaCircleA")
    println("Circumference Circle A: $circumferenceCircleA")
    println("Area Circle B: %.4f".format(areaCircleB))
    println("Circumference Circle B: ${ceil(circumferenceCircle(radiusB))}")
    println("Area Circle C: ${areaCircle(radiusC)}")
    println("Circumference Circle C: ${circumferenceCircle(radiusC)}")
}