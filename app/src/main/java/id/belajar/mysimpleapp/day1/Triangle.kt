package id.belajar.mysimpleapp.day1

fun triangle1(input: Int){
    var stars = input
    for (i in stars downTo 1){
        for (j in 1..stars){
            print("*")
        }
        stars--
        println()
    }
}

fun triangle2(input: Int){
    var stars = input
    for (i in 1..stars){
        for (j in 1..i){
            print("*")
        }
        println()
    }

}

fun main() {
    triangle1(5)
}