package id.belajar.mysimpleapp.day1

fun isPalindrom(input: String) : String{
    val inputReverse = input.reversed()
    return if (input.equals(inputReverse, ignoreCase = true)){
        "Palindrome"
    } else {
        "Not Palindrome"
    }
}

fun main() {
    val anyStuff = "Macam"
    println(isPalindrom(anyStuff))
}